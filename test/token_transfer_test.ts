import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";

import {
  TokenName,
  TokenSymbol,
  TokenDecimals,
  TotalSupply,
  MintAmount,
  FeeReflection,
  FeeDevMarket,
  FeeCharity,
  FeeResourceLiquidity,
  deployContract,
} from "./lib";
import { DevMarketSeed, CharitySeed, ResourceLiquiditySeed } from "./lib";
import { calculateFeeAmount, timeWarp } from "./lib";

describe("Transfers", () => {
  let accounts: Signer[];
  let contract: Contract;
  let test_contract: Contract;

  let owner: string;
  let dev_market_wallet: string;
  let charity_wallet: string;
  let resource_liquidity_wallet: string;

  let alice_signer: Signer;
  let alice_wallet: string;
  let bob_signer: Signer;
  let bob_wallet: string;

  let seed_balance = calculateFeeAmount(MintAmount, 500);

  before(async () => {
    [accounts, contract] = await deployContract();

    owner = await accounts[0].getAddress();
    dev_market_wallet = await accounts[1].getAddress();
    charity_wallet = await accounts[2].getAddress();
    resource_liquidity_wallet = await accounts[3].getAddress();

    alice_signer = accounts[10];
    alice_wallet = await alice_signer.getAddress();

    bob_signer = accounts[11];
    bob_wallet = await bob_signer.getAddress();

    test_contract = await (await ethers.getContractFactory("Test")).deploy();
  });

  beforeEach(async () => {
    [accounts, contract] = await deployContract();
  });

  it("should allow basic transfer", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      expect(await instance.connect(alice_signer).transfer(bob_wallet, 1000)).to
        .be.ok;
    });
  });

  it("should fail when amount greater than available balance", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await expect(
        instance.connect(alice_signer).transfer(bob_wallet, seed_balance.add(1))
      ).to.be.revertedWith("ERC20: transfer amount exceeds senders balance");
    });
  });

  it("should fail when amount == 0", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await expect(
        instance.connect(alice_signer).transfer(bob_wallet, 0)
      ).to.be.revertedWith("ERC20: transfer amount must be greater than zero");
    });
  });

  it("should fail when amount sending to self", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await expect(
        instance.connect(alice_signer).transfer(alice_wallet, seed_balance)
      ).to.be.revertedWith("ERC20: transfer to self");
    });
  });

  it("should not emit expected events on normal transfer", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await expect(
        await instance.connect(alice_signer).transfer(bob_wallet, 1000)
      )
        .to.emit(instance, "Transfer")
        .to.not.emit(instance, "ReflectedAmount")
        .to.not.emit(instance, "DevMarketingFeeApplied")
        .to.not.emit(instance, "CharityFeeApplied")
        .to.not.emit(instance, "ResourceLiquidityFeeApplied");
    });
  });

  it("should emit expected events on transfer to contract", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await expect(
        await instance
          .connect(alice_signer)
          .transfer(test_contract.address, 1000)
      )
        .to.emit(instance, "ReflectedAmount")
        .to.emit(instance, "DevMarketingFeeApplied")
        .to.emit(instance, "CharityFeeApplied")
        .to.emit(instance, "ResourceLiquidityFeeApplied")
        .to.emit(instance, "Transfer");
    });
  });

  it("should have not collected fees from normal address transfer", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await instance
        .connect(alice_signer)
        .transfer(bob_wallet, seed_balance.div(2));

      let dev_market_balance = await instance.balanceOf(dev_market_wallet);
      let charity_balance = await instance.balanceOf(charity_wallet);
      let resource_liquidity_balance = await instance.balanceOf(
        resource_liquidity_wallet
      );

      expect(BigNumber.from(dev_market_balance)).to.equal(DevMarketSeed);
      expect(BigNumber.from(charity_balance)).to.equal(CharitySeed);
      expect(BigNumber.from(resource_liquidity_balance)).to.equal(
        ResourceLiquiditySeed
      );
    });
  });

  it("should have collected fees from contract address transfer", async () => {
    let transfer_amount = seed_balance.div(2);

    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await instance
        .connect(alice_signer)
        .transfer(test_contract.address, transfer_amount);

      let dev_market_balance = await instance.balanceOf(dev_market_wallet);
      let charity_balance = await instance.balanceOf(charity_wallet);
      let resource_liquidity_balance = await instance.balanceOf(
        resource_liquidity_wallet
      );

      expect(BigNumber.from(dev_market_balance)).to.equal(
        DevMarketSeed.add(calculateFeeAmount(transfer_amount, FeeDevMarket))
      );
      expect(BigNumber.from(charity_balance)).to.equal(
        CharitySeed.add(calculateFeeAmount(transfer_amount, FeeCharity))
      );
      expect(BigNumber.from(resource_liquidity_balance)).to.equal(
        ResourceLiquiditySeed.add(
          calculateFeeAmount(transfer_amount, FeeResourceLiquidity)
        )
      );
    });
  });

  it("should have not collected fees from normal address transfer (getTotalFeesCollected)", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await instance
        .connect(alice_signer)
        .transfer(bob_wallet, seed_balance.div(2));

      let fees_collected = await instance.getTotalFeesCollected();

      expect(BigNumber.from(fees_collected[0])).to.equal(0);
      expect(BigNumber.from(fees_collected[1])).to.equal(0);
      expect(BigNumber.from(fees_collected[2])).to.equal(0);
      expect(BigNumber.from(fees_collected[3])).to.equal(0);
    });
  });

  it("should have collected fees from contract address transfer (getTotalFeesCollected)", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions
      await instance
        .connect(alice_signer)
        .transfer(test_contract.address, seed_balance.div(2));

      let fees_collected = await instance.getTotalFeesCollected();

      expect(BigNumber.from(fees_collected[0])).to.equal(
        calculateFeeAmount(seed_balance.div(2), FeeReflection)
      ),
        expect(BigNumber.from(fees_collected[1])).to.equal(
          calculateFeeAmount(seed_balance.div(2), FeeDevMarket)
        ),
        expect(BigNumber.from(fees_collected[2])).to.equal(
          calculateFeeAmount(seed_balance.div(2), FeeCharity)
        ),
        expect(BigNumber.from(fees_collected[3])).to.equal(
          calculateFeeAmount(seed_balance.div(2), FeeResourceLiquidity)
        );
    });
  });

  it("should not emit expected events on excluded address transfer", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions

      await expect(
        await instance.connect(accounts[1]).transfer(bob_wallet, 1000)
      )
        .to.not.emit(instance, "ReflectedAmount")
        .to.not.emit(instance, "DevMarketingFeeApplied")
        .to.not.emit(instance, "CharityFeeApplied")
        .to.not.emit(instance, "ResourceLiquidityFeeApplied");
    });
  });

  it("should have collected no fees from excluded address transfer", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits
      await instance.connect(accounts[0]).transfer(alice_wallet, seed_balance);

      let dev_market_balance = await instance.balanceOf(dev_market_wallet);
      let charity_balance = await instance.balanceOf(charity_wallet);
      let resource_liquidity_balance = await instance.balanceOf(
        resource_liquidity_wallet
      );

      expect(BigNumber.from(dev_market_balance)).to.equal(DevMarketSeed);
      expect(BigNumber.from(charity_balance)).to.equal(CharitySeed);
      expect(BigNumber.from(resource_liquidity_balance)).to.equal(
        ResourceLiquiditySeed
      );
    });
  });

  //it("should take fee from standard transfer and deposit correct ammounts", async () => {});
});
