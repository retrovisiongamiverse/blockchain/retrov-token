import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";

import {
  TokenName,
  TokenSymbol,
  TokenDecimals,
  TokenDecimalDivider,
  MintAmount,
  BurnWallet,
} from "./lib";
import {
  DevMarketSeed,
  CharitySeed,
  ResourceLiquiditySeed,
  OwnerTokenBalance,
} from "./lib";
import { deployContract } from "./lib";

describe("Deployment", () => {
  let accounts: Signer[];

  let owner_wallet: string;
  let dev_market_wallet: string;
  let charity_wallet: string;
  let resource_liquidity: string;

  context("Initial state", () => {
    let contract: Contract;

    before(async () => {
      [accounts, contract] = await deployContract();

      owner_wallet = await accounts[0].getAddress();
      dev_market_wallet = await accounts[1].getAddress();
      charity_wallet = await accounts[2].getAddress();
      resource_liquidity = await accounts[3].getAddress();
    });

    it("should have correct name", async () => {
      contract
        .deployed()
        .then((instance) => instance.name.call())
        .then((name) => {
          expect(name).to.be.eq(TokenName), "name is not " + TokenName;
        });
    });

    it("should have correct symbol", async () => {
      contract
        .deployed()
        .then((instance) => instance.symbol.call())
        .then((symbol) => {
          expect(symbol).to.be.eq(TokenSymbol), "symbol is not " + TokenSymbol;
        });
    });

    it("should have correct decimals", async () => {
      contract
        .deployed()
        .then((instance) => instance.decimals.call())
        .then((decimals) => {
          expect(decimals).to.be.eq(TokenDecimals),
            "decimals is not " + TokenDecimals;
        });
    });

    it("should have 0 collected reflections", async () => {
      contract
        .deployed()
        .then((instance) => instance.totalReflections())
        .then((burned) => {
          expect(burned).to.be.eq(0), "Initial state not zero collected fees";
        });
    });
  });

  context("Initial balances", () => {
    let contract: Contract;

    before(async () => {
      [accounts, contract] = await deployContract();

      owner_wallet = await accounts[0].getAddress();
      dev_market_wallet = await accounts[1].getAddress();
      charity_wallet = await accounts[2].getAddress();
      resource_liquidity = await accounts[3].getAddress();
    });

    it("should have correct total supply", async () => {
      contract
        .deployed()
        .then((instance) => instance.totalSupply())
        .then((totalSupply) => {
          expect(BigNumber.from(totalSupply)).to.equal(MintAmount),
            "contract totalSupply does not equal " + MintAmount + " tokens";
        });
    });

    it(
      "should mint " +
        DevMarketSeed.div(TokenDecimalDivider) +
        " to dev market wallet",
      async () => {
        contract
          .deployed()
          .then((instance) => instance.getInternalBalance(0))
          .then((balance) => {
            expect(BigNumber.from(balance)).to.equal(DevMarketSeed),
              "dev market wallet does not have " +
                DevMarketSeed.div(TokenDecimalDivider) +
                " tokens";
          });
      }
    );

    it(
      "should mint " +
        CharitySeed.div(TokenDecimalDivider) +
        " to charity wallet",
      async () => {
        contract
          .deployed()
          .then((instance) => instance.getInternalBalance(1))
          .then((balance) => {
            expect(BigNumber.from(balance)).to.equal(CharitySeed),
              "charity wallet does not have " +
                CharitySeed.div(TokenDecimalDivider) +
                " tokens";
          });
      }
    );

    it(
      "should mint " +
        ResourceLiquiditySeed.div(TokenDecimalDivider) +
        " to contract for resource liquidity",
      async () => {
        contract
          .deployed()
          .then((instance) => instance.getInternalBalance(2))
          .then((balance) => {
            expect(BigNumber.from(balance)).to.equal(ResourceLiquiditySeed),
              "resource liquidity does not have " +
                ResourceLiquiditySeed.div(TokenDecimalDivider) +
                " tokens";
          });
      }
    );

    it(
      "should mint " +
        OwnerTokenBalance.div(TokenDecimalDivider) +
        " to owner wallet",
      async () => {
        contract
          .deployed()
          .then((instance) => instance.balanceOf(owner_wallet))
          .then((balance) => {
            expect(BigNumber.from(balance)).to.equal(OwnerTokenBalance),
              "owner wallet does not have " +
                OwnerTokenBalance.div(TokenDecimalDivider) +
                " tokens";
          });
      }
    );
  });

  context("Go Live", () => {
    let contract: Contract;

    before(async () => {
      [accounts, contract] = await deployContract();

      owner_wallet = await accounts[0].getAddress();
      dev_market_wallet = await accounts[1].getAddress();
      charity_wallet = await accounts[2].getAddress();
      resource_liquidity = await accounts[3].getAddress();
    });

    it("should not be live", async () => {
      contract
        .deployed()
        .then((instance) => instance.isLive())
        .then((isLive) => {
          expect(isLive).to.equal(false), "token is live when it should not be";
        });
    });

    it("should emit GoLive event", async () => {
      contract.deployed().then(async (instance) => {
        await expect(await instance.goLive()).to.emit(instance, "GoLive");
      });
    });

    it("should revert second goLive call", async () => {
      contract.deployed().then(async (instance) => {
        await expect(instance.goLive()).to.be.revertedWith(
          "ERC20: Token already live"
        );
      });
    });
  });

  context("Initial Fees and Fee Updates", () => {
    let contract: Contract;

    let initial_reflection_fee = 300;
    let initial_dev_market_fee = 200;
    let initial_charity_fee = 200;
    let initial_resource_liquidity_fee = 200;

    before(async () => {
      [accounts, contract] = await deployContract();

      owner_wallet = await accounts[0].getAddress();
      dev_market_wallet = await accounts[1].getAddress();
      charity_wallet = await accounts[2].getAddress();
      resource_liquidity = await accounts[3].getAddress();
    });

    it(
      "should default reflection fee to " + initial_reflection_fee / 100 + "%",
      async () => {
        contract
          .deployed()
          .then((instance) => instance.getFee(0))
          .then((fee) => {
            expect(BigNumber.from(fee)).to.equal(initial_reflection_fee),
              "initial reflection fee is not " +
                initial_reflection_fee / 100 +
                "%";
          });
      }
    );

    it(
      "should default dev marketing fee to " +
        initial_dev_market_fee / 100 +
        "%",
      async () => {
        contract
          .deployed()
          .then((instance) => instance.getFee(1))
          .then((fee) => {
            expect(BigNumber.from(fee)).to.equal(initial_dev_market_fee),
              "initial dev marketing fee is not " +
                initial_dev_market_fee / 100 +
                "%";
          });
      }
    );

    it(
      "should default charity fee to " + initial_charity_fee / 100 + "%",
      async () => {
        contract
          .deployed()
          .then((instance) => instance.getFee(2))
          .then((fee) => {
            expect(BigNumber.from(fee)).to.equal(initial_charity_fee),
              "initial charity fee is not " + initial_charity_fee / 100 + "%";
          });
      }
    );

    it("should fetch default fees from getFees", async () => {
      contract
        .deployed()
        .then((instance) => instance.getFees())
        .then((fees) => {
          expect(BigNumber.from(fees[0])).to.equal(initial_reflection_fee),
            "initial reflection fee is not " +
              initial_reflection_fee / 100 +
              "%";

          expect(BigNumber.from(fees[1])).to.equal(initial_dev_market_fee),
            "initial dev marklet fee is not " +
              initial_dev_market_fee / 100 +
              "%";

          expect(BigNumber.from(fees[2])).to.equal(initial_charity_fee),
            "initial charity fee is not " + initial_reflection_fee / 100 + "%";

          expect(BigNumber.from(fees[3])).to.equal(
            initial_resource_liquidity_fee
          ),
            "initial resource liquidity fee is not " +
              initial_resource_liquidity_fee / 100 +
              "%";
        });
    });

    it(
      "should default resource liquidity fee to " +
        initial_resource_liquidity_fee / 100 +
        "%",
      async () => {
        contract
          .deployed()
          .then((instance) => instance.getFee(3))
          .then((fee) => {
            expect(BigNumber.from(fee)).to.equal(
              initial_resource_liquidity_fee
            ),
              "initial resource liquidity fee is not " +
                initial_resource_liquidity_fee / 100 +
                "%";
          });
      }
    );

    it("should update reflection fee to " + 100 / 100 + "%", async () => {
      contract.deployed().then(async (instance) => {
        await expect(instance.setFee(0, 100))
          .to.emit(instance, "FeeUpdated")
          .withArgs(0, initial_reflection_fee, 100);

        let fee = await instance.getFee(0);
        expect(BigNumber.from(fee)).to.equal(100),
          "reflection fee is not " + 100 / 100 + "%";
      });
    });

    it(
      "should fail to update reflection fee to " +
        100 / 100 +
        "% from " +
        100 / 100 +
        "%",
      async () => {
        contract.deployed().then(async (instance) => {
          await expect(instance.setFee(0, 100)).to.be.revertedWith(
            "ERC20: fee already set"
          );
        });
      }
    );

    it(
      "should fail to update reflection fee to " + 2100 / 100 + "%",
      async () => {
        contract.deployed().then(async (instance) => {
          await expect(instance.setFee(0, 2100)).to.be.revertedWith(
            "ERC20: proposed fee schedule exceeds limit of 20%"
          );
        });
      }
    );
  });

  context("Initial Addresses and Updates", () => {
    let contract: Contract;
    let test_contract: Contract;

    before(async () => {
      [accounts, test_contract] = await deployContract();

      owner_wallet = await accounts[0].getAddress();
      dev_market_wallet = await accounts[1].getAddress();
      charity_wallet = await accounts[2].getAddress();
      resource_liquidity = await accounts[3].getAddress();
    });

    beforeEach(async () => {
      [accounts, contract] = await deployContract();
    });

    it("should set mutable internal wallets", () => {
      [1, 2, 3].forEach(async (idx) => {
        let wallet = await accounts[idx].getAddress();

        contract
          .deployed()
          .then((instance) => instance.getInternalWalletAddress(idx - 1))
          .then((response: [String, Boolean]) => {
            expect(response[0]).to.be.eq(wallet);
            expect(response[1]).to.be.eq(false);
          });
      });
    });

    it("should update dev marketing wallet to new address", async () => {
      let new_address = await accounts[9].getAddress();

      contract.deployed().then(async (instance) => {
        await expect(instance.setInternalWalletAddress(0, new_address))
          .to.emit(instance, "InternalWalletAddressChanged")
          .withArgs(0, dev_market_wallet, new_address);
      });
    });

    it("should NOT update dev marketing wallet to 0x0", async () => {
      contract.deployed().then(async (instance) => {
        await expect(
          instance.setInternalWalletAddress(0, BurnWallet)
        ).to.be.revertedWith("ERC20: internal wallet must not be 0");
      });
    });

    it("should NOT update dev marketing wallet to this contract", async () => {
      contract.deployed().then(async (instance) => {
        await expect(
          instance.setInternalWalletAddress(0, contract.address)
        ).to.be.revertedWith(
          "ERC20: internal wallet must not be this contract"
        );
      });
    });

    it("should NOT update dev marketing wallet to current", async () => {
      let dev_marketing_wallet = await accounts[1].getAddress();

      contract.deployed().then(async (instance) => {
        await expect(
          instance.setInternalWalletAddress(0, dev_marketing_wallet)
        ).to.be.revertedWith("ERC20: new address must not be same");
      });
    });

    it("should update dev marketing wallet to contract and make immutable", async () => {
      let dev_marketing_wallet = await accounts[1].getAddress();

      contract.deployed().then(async (instance) => {
        let new_address = (await test_contract.deployed()).address;

        await expect(instance.setInternalWalletAddress(0, new_address))
          .to.emit(instance, "InternalWalletAddressMadeImmutable")
          .withArgs(0, dev_marketing_wallet, new_address);

        await expect(
          instance.setInternalWalletAddress(0, dev_marketing_wallet)
        ).to.be.revertedWith("ERC20: internal address is immutable");
      });
    });
  });
});
