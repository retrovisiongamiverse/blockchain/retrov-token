import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";

import {
  TokenName,
  TokenSymbol,
  TokenDecimals,
  TokenDecimalDivider,
  TotalSupply,
  deployContract,
} from "./lib";

describe("Allowances", () => {
  let accounts: Signer[];
  let contract: Contract;

  let owner: string;
  let dev_market_wallet: string;
  let charity_wallet: string;
  let normal_wallet: string;

  let allowance = BigNumber.from(1000).mul(TokenDecimalDivider);

  before(async () => {
    accounts = await ethers.getSigners();
    owner = await accounts[0].getAddress();
    dev_market_wallet = await accounts[1].getAddress();
    charity_wallet = await accounts[2].getAddress();
    normal_wallet = await accounts[9].getAddress();
  });

  beforeEach(async () => {
    [accounts, contract] = await deployContract();
  });

  it("should allow sender to set allowance to address (UNSAFE)", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(instance.approve(normal_wallet, allowance)).to.emit(
        instance,
        "Approval"
      );

      let approvedAllowance = BigNumber.from(
        await instance.allowance(owner, normal_wallet)
      );
      expect(approvedAllowance).to.equal(allowance), "allowance does not match";
    });
  });

  it("should allow sender to increase allowance for address", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(
        instance.increaseAllowance(normal_wallet, allowance)
      ).to.emit(instance, "Approval");

      let approvedAllowance = BigNumber.from(
        await instance.allowance(owner, normal_wallet)
      );
      expect(approvedAllowance).to.equal(allowance), "allowance does not match";
    });
  });

  it("should allow sender to decrease allowance to address after increased", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(
        instance.increaseAllowance(normal_wallet, allowance)
      ).to.emit(instance, "Approval");

      await expect(
        instance.decreaseAllowance(normal_wallet, allowance.div(2))
      ).to.emit(instance, "Approval");

      let approvedAllowance = BigNumber.from(
        await instance.allowance(owner, normal_wallet)
      );
      expect(approvedAllowance).to.equal(allowance.div(2)),
        "allowance does not match";
    });
  });

  it("should fail to decrease allowance to address below zero", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(
        instance.decreaseAllowance(normal_wallet, allowance)
      ).to.be.revertedWith("ERC20: decreased allowance below zero");
    });
  });

  it("should allow transferFrom owner as normal_wallet account ", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.increaseAllowance(normal_wallet, allowance);

      await expect(
        instance
          .connect(accounts[9])
          .transferFrom(owner, normal_wallet, allowance)
      ).to.emit(instance, "Transfer");

      let remainingAllowance = BigNumber.from(
        await instance.allowance(owner, normal_wallet)
      );

      expect(await instance.balanceOf(normal_wallet)).to.equal(allowance);
      expect(remainingAllowance).to.equal(0);
    });
  });

  it("should fail transferFrom owner as normal_wallet account when greater than allowance", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.increaseAllowance(normal_wallet, allowance);
      await expect(
        instance
          .connect(accounts[9])
          .transferFrom(owner, normal_wallet, allowance.mul(2))
      ).to.be.revertedWith("ERC20: transfer amount exceeds allowance");
    });
  });
});
