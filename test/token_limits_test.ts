import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";

import {
  TokenName,
  TokenSymbol,
  TokenDecimals,
  TotalSupply,
  MintAmount,
  deployContract,
} from "./lib";
import { thirtyDays } from "./lib";
import { calculateFeeAmount, timeWarp } from "./lib";

describe("Holder limits", () => {
  let accounts: Signer[];
  let contract: Contract;

  let owner: string;
  let dev_market_wallet: string;
  let charity_wallet: string;
  let normal_wallet: string;

  let holdingCap0Days = calculateFeeAmount(MintAmount, BigNumber.from(50));
  let holdingCap30Days = calculateFeeAmount(MintAmount, BigNumber.from(100));
  let holdingCap60Days = calculateFeeAmount(MintAmount, BigNumber.from(500));
  let holdingCap90Days = calculateFeeAmount(MintAmount, BigNumber.from(1000));

  before(async () => {
    [accounts, contract] = await deployContract();

    owner = await accounts[0].getAddress();
    dev_market_wallet = await accounts[1].getAddress();
    charity_wallet = await accounts[2].getAddress();
    normal_wallet = await accounts[9].getAddress();

    contract.deployed().then((instance) => instance.goLive());
  });

  it("should exclude owner from cap limit", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.getHoldingCap(dev_market_wallet)).to.be.equal(
        MintAmount
      );
    });
  });

  it("should exclude dev marketing from cap limit", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.getHoldingCap(dev_market_wallet)).to.be.equal(
        MintAmount
      );
    });
  });

  it("should exclude charity from cap limit", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.getHoldingCap(dev_market_wallet)).to.be.equal(
        MintAmount
      );
    });
  });

  it("should have default holding cap of 0.5% within first 30 days", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.getHoldingCap(normal_wallet)).to.be.equal(
        holdingCap0Days
      );
    });
  });

  it("should have default holding cap of 1.0% between 30 and 60 days", async () => {
    return contract.deployed().then(async (instance) => {
      await timeWarp(thirtyDays);
      expect(await instance.getHoldingCap(normal_wallet)).to.be.equal(
        holdingCap30Days
      );
    });
  });

  it("should have default holding cap of 5.0% between 60 and 90 days", async () => {
    return contract.deployed().then(async (instance) => {
      await timeWarp(thirtyDays);
      expect(await instance.getHoldingCap(normal_wallet)).to.be.equal(
        holdingCap60Days
      );
    });
  });

  it("should have default holding cap of 10.0% at 90 days", async () => {
    return contract.deployed().then(async (instance) => {
      await timeWarp(thirtyDays);
      expect(await instance.getHoldingCap(normal_wallet)).to.be.equal(
        holdingCap90Days
      );
    });
  });

  it("should have no holding cap after 180+ days", async () => {
    return contract.deployed().then(async (instance) => {
      await timeWarp(thirtyDays * 100);
      expect(await instance.getHoldingCap(normal_wallet)).to.be.equal(
        MintAmount
      );
    });
  });
});

describe("TX limits", () => {
  let accounts: Signer[];
  let contract: Contract;

  let owner: string;
  let dev_market_wallet: string;
  let charity_wallet: string;
  let normal_wallet: string;

  before(async () => {
    [accounts, contract] = await deployContract();

    owner = await accounts[0].getAddress();
    dev_market_wallet = await accounts[1].getAddress();
    charity_wallet = await accounts[2].getAddress();
    normal_wallet = await accounts[9].getAddress();

    contract.deployed().then((instance) => instance.goLive());
  });

  it("should exclude owner from TX limit", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.getTXCap(dev_market_wallet)).to.be.equal(
        MintAmount
      );
    });
  });

  it("should exclude dev marketing from TX limit", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.getTXCap(dev_market_wallet)).to.be.equal(
        MintAmount
      );
    });
  });

  it("should exclude charity from TX limit", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.getTXCap(dev_market_wallet)).to.be.equal(
        MintAmount
      );
    });
  });

  it("should limit TX to 10% of holding limit for first 30 days", async () => {
    return contract.deployed().then(async (instance) => {
      let holdingCap = await instance.getHoldingCap(normal_wallet);
      expect(await instance.getTXCap(normal_wallet)).to.be.equal(
        calculateFeeAmount(holdingCap, BigNumber.from(1000))
      );
    });
  });

  it("should limit TX to 25% of holding limit between 30 and 60 days", async () => {
    return contract.deployed().then(async (instance) => {
      await timeWarp(thirtyDays);
      let holdingCap = await instance.getHoldingCap(normal_wallet);
      expect(await instance.getTXCap(normal_wallet)).to.be.equal(
        calculateFeeAmount(holdingCap, BigNumber.from(2500))
      );
    });
  });

  it("should limit TX to 50% of holding limit between 60 and 90 days", async () => {
    return contract.deployed().then(async (instance) => {
      await timeWarp(thirtyDays);
      let holdingCap = await instance.getHoldingCap(normal_wallet);
      expect(await instance.getTXCap(normal_wallet)).to.be.equal(
        calculateFeeAmount(holdingCap, BigNumber.from(5000))
      );
    });
  });

  it("should limit TX to 100% of holding limit after 90 days", async () => {
    return contract.deployed().then(async (instance) => {
      await timeWarp(thirtyDays);
      let holdingCap = await instance.getHoldingCap(normal_wallet);
      expect(await instance.getTXCap(normal_wallet)).to.be.equal(
        calculateFeeAmount(holdingCap, BigNumber.from(10000))
      );
    });
  });

  it("should limit TX to 100% of holding limit after 3000+ days", async () => {
    return contract.deployed().then(async (instance) => {
      await timeWarp(thirtyDays * 100);
      let holdingCap = await instance.getHoldingCap(normal_wallet);
      expect(await instance.getTXCap(normal_wallet)).to.be.equal(
        calculateFeeAmount(holdingCap, BigNumber.from(10000))
      );
    });
  });
});
