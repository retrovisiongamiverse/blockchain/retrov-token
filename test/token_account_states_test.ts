import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";

import {
  TokenName,
  TokenSymbol,
  TokenDecimals,
  TotalSupply,
  MintAmount,
  deployContract,
} from "./lib";
import { calculateFeeAmount, timeWarp } from "./lib";

describe("Account states", () => {
  let accounts: Signer[];
  let contract: Contract;

  let owner: string;
  let dev_market_wallet: string;
  let charity_wallet: string;

  let alice_signer: Signer;
  let alice_wallet: string;
  let bob_signer: Signer;
  let bob_wallet: string;
  let carol_signer: Signer;
  let carol_wallet: string;
  let dave_signer: Signer;
  let dave_wallet: string;

  let seed_balance = calculateFeeAmount(MintAmount, 500);

  before(async () => {
    accounts = await ethers.getSigners();
    owner = await accounts[0].getAddress();
    dev_market_wallet = await accounts[1].getAddress();
    charity_wallet = await accounts[2].getAddress();

    alice_signer = accounts[5];
    alice_wallet = await alice_signer.getAddress();

    bob_signer = accounts[6];
    bob_wallet = await bob_signer.getAddress();

    carol_signer = accounts[7];
    carol_wallet = await carol_signer.getAddress();

    dave_signer = accounts[8];
    dave_wallet = await dave_signer.getAddress();
  });

  beforeEach(async () => {
    [accounts, contract] = await deployContract();
  });

  it("should permit owner account transfer before goLive", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.transfer(alice_wallet, seed_balance);
      expect(await instance.balanceOf(alice_wallet)).to.equal(seed_balance);
    });
  });

  it("should prevent non-owner account transfer before goLive", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.transfer(alice_wallet, seed_balance);

      await expect(
        instance
          .connect(alice_signer)
          .transfer(bob_wallet, seed_balance.div(10))
      ).to.be.revertedWith("ERC20: token not yet live on-chain");
    });
  });

  it("should emit AccountExclusionStatusChanged", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(
        instance.setAccountExcluded(carol_wallet, true, "Test: add exclusion")
      ).to.emit(instance, "AccountExclusionStatusChanged");

      await expect(await instance.isExcluded(carol_wallet)).to.equal(true);

      await expect(
        instance.setAccountExcluded(
          carol_wallet,
          false,
          "Test: remove exclusion"
        )
      ).to.emit(instance, "AccountExclusionStatusChanged");

      await expect(await instance.isExcluded(carol_wallet)).to.equal(false);
    });
  });

  it("should permit excluded account send over TX cap", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.transfer(alice_wallet, seed_balance);
      await instance.setAccountExcluded(
        carol_wallet,
        true,
        "Test: add exclusion TX no-cap test"
      );

      // TODO: Test
      expect(await instance.balanceOf(alice_wallet)).to.equal(seed_balance);
    });
  });

  it("should permit excluded account receive over holder cap", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.transfer(alice_wallet, seed_balance);
      await instance.setAccountExcluded(
        carol_wallet,
        true,
        "Test: add exclusion for no-cap receipt"
      );

      // TODO: Test
      // Perform transfer from Alice to Carol
      expect(await instance.balanceOf(alice_wallet)).to.equal(seed_balance);
    });
  });

  it("should emit AccountLockedStatusChanged", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(
        instance.setAccountLocked(carol_wallet, true, "Test: add lock")
      ).to.emit(instance, "AccountLockedStatusChanged");

      await expect(await instance.isLocked(carol_wallet)).to.equal(true);

      await expect(
        instance.setAccountLocked(carol_wallet, false, "Test: remove lock")
      ).to.emit(instance, "AccountLockedStatusChanged");

      await expect(await instance.isLocked(carol_wallet)).to.equal(false);
    });
  });

  it("should prevent new account from transfering first 24 hours", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive();

      await instance.transfer(alice_wallet, seed_balance);

      await timeWarp(180);

      await expect(
        instance.connect(alice_signer).transfer(bob_wallet, 1000000000)
      ).to.be.revertedWith(
        "ERC20: transfer failed, intial wait period not met"
      );

      await timeWarp(60 * 60 * 24);

      expect(instance.connect(alice_signer).transfer(bob_wallet, 1000000000));
    });
  });

  it("should prevent arbitrage for 90s", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive();

      await instance.transfer(alice_wallet, seed_balance);

      await timeWarp(60 * 60 * 24);

      await expect(
        instance.connect(alice_signer).transfer(bob_wallet, 1000000000)
      ).to.be.ok;

      await timeWarp(15);

      expect(
        instance.connect(alice_signer).transfer(bob_wallet, 1000000000)
      ).to.be.revertedWith("ERC20: transfer failed, arbitrage delay not met");
    });
  });

  it("should return expected values for call to getRemainingTXWait", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive();

      // Initial account wait period
      await instance.transfer(alice_wallet, seed_balance);
      await timeWarp(60 * 60 * 10); // Fast forward 10h

      let remainingTXWait = await instance.getRemainingTXWait(alice_wallet);
      expect(remainingTXWait).to.be.within(89, 60 * 60 * 24 + 1);

      // console.log("Remaining TX wait: " + remainingTXWait);
      await timeWarp(60 * 60 * 24); // Fast forward past initial wait period

      // Verify arbitrage delay
      await instance.connect(alice_signer).transfer(bob_wallet, 1000000000);
      remainingTXWait = await instance.getRemainingTXWait(alice_wallet);
      expect(remainingTXWait).to.be.below(91);

      // console.log("Remaining TX wait: " + remainingTXWait);

      await timeWarp(60);

      remainingTXWait = await instance.getRemainingTXWait(alice_wallet);
      expect(remainingTXWait).to.be.below(60);

      // Verify arbitrage delay expires
      await timeWarp(30);
      remainingTXWait = await instance.getRemainingTXWait(alice_wallet);
      expect(remainingTXWait).to.equal(0);
    });
  });

  it("should return 0 for getRemainingTXWait on excluded", async () => {
    return contract.deployed().then(async (instance) => {
      let remainingTXWait = await instance.getRemainingTXWait(
        dev_market_wallet
      );
      expect(remainingTXWait).to.equal(0);
    });
  });

  it("should prevent locked account from transfering", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive();

      await instance.transfer(alice_wallet, seed_balance);

      await timeWarp(60 * 60 * 24);

      await instance.setAccountLocked(
        alice_wallet,
        true,
        "Test: locked transfer"
      );

      await expect(
        instance
          .connect(alice_signer)
          .transfer(bob_wallet, seed_balance.div(10))
      ).to.be.revertedWith("ERC20: transfer failed, account is locked");
    });
  });

  it("should emit AccountCreated on newly tracked address", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive();

      await expect(
        instance.transfer(alice_wallet, seed_balance.div(2))
      ).to.emit(instance, "AccountCreated");
      await expect(
        instance.transfer(alice_wallet, seed_balance.div(2))
      ).to.not.emit(instance, "AccountCreated");
    });
  });
});
