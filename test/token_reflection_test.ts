import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";

import {
  TokenName,
  TokenSymbol,
  TokenDecimals,
  TotalSupply,
  MintAmount,
  FeeDevMarket,
  FeeCharity,
  FeeResourceLiquidity,
  deployContract,
} from "./lib";
import { FeeReflection } from "./lib";
import { calculateBP, calculateFeeAmount, timeWarp } from "./lib";

describe("Reflections", () => {
  let accounts: Signer[];
  let test_contract: Contract;
  let contract: Contract;

  let owner: string;
  let dev_market_wallet: string;
  let charity_wallet: string;
  let resource_liquidity_wallet: string;

  let alice_signer: Signer;
  let alice_wallet: string;
  let bob_signer: Signer;
  let bob_wallet: string;
  let carol_signer: Signer;
  let carol_wallet: string;
  let dave_signer: Signer;
  let dave_wallet: string;

  function calculateReflectionOwed(
    balance: BigNumber,
    reflectionsBalance: BigNumber,
    lastReflectedTick: BigNumber,
    reflectionTicks: BigNumber
  ): BigNumber {
    let ownershipBP: BigNumber = calculateBP(balance, MintAmount);
    let reflectionBP: BigNumber = calculateBP(
      reflectionTicks.sub(lastReflectedTick),
      reflectionTicks
    );
    let reflectionShare: BigNumber = calculateFeeAmount(
      reflectionsBalance,
      reflectionBP
    );

    return calculateFeeAmount(reflectionShare, ownershipBP);
  }

  before(async () => {
    [accounts, contract] = await deployContract();
    owner = await accounts[0].getAddress();
    dev_market_wallet = await accounts[1].getAddress();
    charity_wallet = await accounts[2].getAddress();
    resource_liquidity_wallet = await accounts[3].getAddress();

    alice_signer = accounts[5];
    alice_wallet = await alice_signer.getAddress();

    bob_signer = accounts[6];
    bob_wallet = await bob_signer.getAddress();

    carol_signer = accounts[7];
    carol_wallet = await carol_signer.getAddress();

    dave_signer = accounts[8];
    dave_wallet = await dave_signer.getAddress();
  });

  beforeEach(async () => {
    [accounts, contract] = await deployContract();
    [accounts, test_contract] = await deployContract();
  });

  it("should not take reflections from transfer between normal accounts", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits

      let seed_balance = calculateFeeAmount(MintAmount, 200);

      await instance.transfer(alice_wallet, seed_balance);

      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions

      let alice_balance = await instance.balanceOf(alice_wallet);
      let alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);

      let bob_balance = await instance.balanceOf(bob_wallet);
      let bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      let reflection_balance = await instance.totalReflections();

      // Seed balances
      let transferAmount = seed_balance.div(4);

      // console.log(`Alice sends ${transferAmount} of her ${alice_balance} to Bob`);

      await instance.connect(alice_signer).transfer(bob_wallet, transferAmount);

      alice_balance = await instance.balanceOf(alice_wallet);
      alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      // console.log(`     Alice Balance:   ${alice_balance} r: ${alice_rBalance}`);

      bob_balance = await instance.balanceOf(bob_wallet);
      bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);
      // console.log(`       Bob Balance:   ${bob_balance} r: ${bob_rBalance}`);

      reflection_balance = await instance.totalReflections();
      // console.log(`Reflection Balance:   ${reflection_balance}`);

      let reflectionFeeAmount = calculateFeeAmount(
        transferAmount,
        FeeReflection
      );
      expect(reflection_balance).to.equal(0);
    });
  });

  it("should show no reflections owed after TX to new account (Bob)", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits

      let seed_balance = calculateFeeAmount(MintAmount, 200);

      await instance.transfer(alice_wallet, seed_balance);

      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions

      let alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      let bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      // Seed balances
      let transferAmount = seed_balance.div(4);

      await instance.connect(alice_signer).transfer(bob_wallet, transferAmount);

      alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      expect(alice_rBalance).to.equal(0);
      expect(bob_rBalance).to.equal(0);
    });
  });

  it("should equal expected reflection ", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits

      let seed_balance = calculateFeeAmount(MintAmount, 200);

      await instance.transfer(alice_wallet, seed_balance);
      await instance.transfer(bob_wallet, seed_balance);

      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions

      let alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      let bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      expect(alice_rBalance).to.equal(0);
      expect(bob_rBalance).to.equal(0);

      // Seed balances
      let transferAmount = seed_balance.div(4);

      await instance.connect(alice_signer).transfer(bob_wallet, transferAmount);

      alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      expect(alice_rBalance).to.equal(0);
      expect(bob_rBalance).to.equal(0);

      await timeWarp(90);

      await instance
        .connect(alice_signer)
        .transfer(test_contract.address, transferAmount); // Transfer to contract to simulate sale

      alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      let bob_balance = await instance.balanceOf(bob_wallet);
      let reflection_balance = await instance.totalReflections();

      let bob_expected_rBalance = calculateReflectionOwed(
        bob_balance,
        reflection_balance,
        BigNumber.from(2),
        BigNumber.from(1)
      );

      expect(alice_rBalance).to.equal(0);
      expect(bob_rBalance).to.not.equal(bob_expected_rBalance);
    });
  });

  it("should have reflection balance of 0 between normal accounts", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits

      let seed_balance = calculateFeeAmount(MintAmount, 200);

      await instance.transfer(alice_wallet, seed_balance);
      await instance.transfer(bob_wallet, seed_balance);

      await timeWarp(60 * 60 * 24); // Fastforward a day to avoid account restrictions

      let alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      let bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      expect(alice_rBalance).to.equal(0);
      expect(bob_rBalance).to.equal(0);

      // Seed balances
      let transferAmount = seed_balance.div(4);

      await instance.connect(alice_signer).transfer(bob_wallet, transferAmount);

      alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      expect(alice_rBalance).to.equal(0);
      expect(bob_rBalance).to.equal(0);

      await timeWarp(90);

      await instance.connect(alice_signer).transfer(bob_wallet, transferAmount);

      alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      expect(alice_rBalance).to.equal(0);
      expect(bob_rBalance).to.equal(0);
    });
  });

  it("should not change circulation amount", async () => {
    return contract.deployed().then(async (instance) => {
      await instance.goLive(); // Token must be live for TX tests to function
      await timeWarp(60 * 60 * 24 * 365); // Fastforward a year to avoid cap/tx limits

      let seed_balance = calculateFeeAmount(MintAmount, 1000);

      await instance.transfer(alice_wallet, seed_balance);
      await instance.transfer(bob_wallet, seed_balance);
      await instance.transfer(carol_wallet, seed_balance);
      await instance.transfer(dave_wallet, seed_balance);

      await timeWarp(60 * 60 * 24 * 180); // Fastforward a 6 months to avoid account restrictions

      let alice_balance = await instance.balanceOf(alice_wallet);
      let alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);

      let bob_balance = await instance.balanceOf(bob_wallet);
      let bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);

      let carol_balance = await instance.balanceOf(carol_wallet);
      let carol_rBalance = await instance.reflectionBalanceOf(carol_wallet);

      let dave_balance = await instance.balanceOf(dave_wallet);
      let dave_rBalance = await instance.reflectionBalanceOf(dave_wallet);

      let reflection_balance = await instance.totalReflections();
      let dev_market_balance = await instance.balanceOf(dev_market_wallet);
      let charity_balance = await instance.balanceOf(charity_wallet);
      let resource_liquidity_balance = await instance.balanceOf(
        instance.address
      );

      // Make some TXs
      for (let i = 0; i < 5; i++) {
        await timeWarp(90);
        await instance
          .connect(alice_signer)
          .transfer(
            test_contract.address,
            seed_balance.div(Math.floor(Math.random() * 15) + 10)
          );
        await timeWarp(90);
        await instance
          .connect(bob_signer)
          .transfer(
            test_contract.address,
            seed_balance.div(Math.floor(Math.random() * 15) + 10)
          );
        await timeWarp(90);
        await instance
          .connect(bob_signer)
          .transfer(
            test_contract.address,
            seed_balance.div(Math.floor(Math.random() * 15) + 10)
          );
        await timeWarp(90);
        await instance
          .connect(dave_signer)
          .transfer(
            test_contract.address,
            seed_balance.div(Math.floor(Math.random() * 15) + 10)
          );
        await timeWarp(90);
        await instance
          .connect(alice_signer)
          .transfer(
            test_contract.address,
            seed_balance.div(Math.floor(Math.random() * 15) + 10)
          );
        await timeWarp(90);
        await instance
          .connect(carol_signer)
          .transfer(
            test_contract.address,
            seed_balance.div(Math.floor(Math.random() * 15) + 10)
          );
        await timeWarp(90);
      }

      // Check total tokens in circulation
      alice_balance = await instance.balanceOf(alice_wallet);
      alice_rBalance = await instance.reflectionBalanceOf(alice_wallet);
      // console.log(`     Alice Balance:   ${alice_balance} r: ${alice_rBalance}`);

      bob_balance = await instance.balanceOf(bob_wallet);
      bob_rBalance = await instance.reflectionBalanceOf(bob_wallet);
      // console.log(`       Bob Balance:   ${bob_balance} r: ${bob_rBalance}`);

      carol_balance = await instance.balanceOf(carol_wallet);
      carol_rBalance = await instance.reflectionBalanceOf(carol_wallet);
      // console.log(`     Carol Balance:   ${carol_balance} r: ${carol_rBalance}`);

      dave_balance = await instance.balanceOf(dave_wallet);
      dave_rBalance = await instance.reflectionBalanceOf(dave_wallet);
      // console.log(`      Dave Balance:   ${dave_balance} r: ${dave_rBalance}`);

      let contract_balance = await instance.balanceOf(test_contract.address);
      let contract_rBalance = await instance.reflectionBalanceOf(
        test_contract.address
      );

      dev_market_balance = await instance.balanceOf(dev_market_wallet);
      charity_balance = await instance.balanceOf(charity_wallet);
      resource_liquidity_balance = await instance.balanceOf(
        resource_liquidity_wallet
      );

      // console.log(`        Dev Market Balance:   ${dev_market_balance}`);
      // console.log(`           Charity Balance:   ${charity_balance}`);
      // console.log(`Resource Liquidity Balance:  ${resource_liquidity_balance}`);

      reflection_balance = await instance.totalReflections();
      // console.log(`Reflection Balance:    ${reflection_balance}`);

      let owner_balance = await instance.balanceOf(owner);
      let owner_rbalance = await instance.reflectionBalanceOf(owner);
      // console.log(`     Owner Balance:  ${owner_balance}`);

      let total_rBalance = reflection_balance
        .sub(alice_rBalance)
        .sub(bob_rBalance)
        .sub(carol_rBalance)
        .sub(dave_rBalance)
        .sub(contract_rBalance)
        .sub(owner_rbalance);
      let total_balance = alice_balance
        .add(bob_balance)
        .add(carol_balance)
        .add(dave_balance)
        .add(dev_market_balance)
        .add(charity_balance)
        .add(resource_liquidity_balance)
        .add(owner_balance)
        .add(total_rBalance)
        .add(contract_balance);

      // console.log(` Calculated Supply: ${total_balance}`);
      // console.log(`      Total Supply: ${MintAmount}`)

      expect(BigNumber.from(MintAmount)).to.equal(total_balance),
        "Total supply not correct";
    });
  });
});
