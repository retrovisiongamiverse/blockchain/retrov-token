import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";

import { MintAmount, deployContract } from "./lib";
import { calculateFeeAmount, timeWarp } from "./lib";

describe("Ownership states", () => {
  let accounts: Signer[];
  let contract: Contract;

  let owner: string;
  let dev_market_wallet: string;
  let charity_wallet: string;

  let alice_signer: Signer;
  let alice_wallet: string;

  let seed_balance = calculateFeeAmount(MintAmount, 500);

  before(async () => {
    accounts = await ethers.getSigners();
    owner = await accounts[0].getAddress();
    dev_market_wallet = await accounts[1].getAddress();
    charity_wallet = await accounts[2].getAddress();

    alice_signer = accounts[5];
    alice_wallet = await alice_signer.getAddress();
  });

  beforeEach(async () => {
    [accounts, contract] = await deployContract();
  });

  it("should not permit non-owner to change ownership", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(
        instance.connect(alice_signer).transferOwnership(alice_wallet)
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });
  });

  it("should not permit non-owner to renounce ownership", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(
        instance.connect(alice_signer).renounceOwnership()
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });
  });

  it("should permit owner account to change ownership", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.transferOwnership(alice_wallet)).to.emit(
        instance,
        "OwnershipTransferred"
      );
    });
  });

  it("should remove exclusion from previous and add for new owner", async () => {
    return contract.deployed().then(async (instance) => {
      await expect(instance.transferOwnership(alice_wallet)).to.emit(
        instance,
        "AccountExclusionStatusChanged"
      );
    });
  });

  it("should permit owner account to renounce ownership", async () => {
    return contract.deployed().then(async (instance) => {
      expect(await instance.renounceOwnership()).to.emit(
        instance,
        "OwnershipTransferred"
      );
    });
  });
});
