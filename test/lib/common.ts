import { HardhatRuntimeEnvironment } from "hardhat/types";
import { ethers } from "hardhat";
import { BigNumber, BigNumberish, Contract, Signer } from "ethers";

export const TokenName = "RetroVision Games";
export const TokenSymbol = "RETROV";
export const TokenDecimals = 9;

export const TokenDecimalDivider = BigNumber.from(10).pow(TokenDecimals);
export const TotalSupply = BigNumber.from("10000000000");
export const MintAmount = TotalSupply.mul(TokenDecimalDivider);
export const BurnWallet = "0x0000000000000000000000000000000000000000";

export const FeeReflection = 300;
export const FeeDevMarket = 200;
export const FeeCharity = 200;
export const FeeResourceLiquidity = 200;

// Wallet seed values
export const DevMarketSeed = calculateFeeAmount(
  MintAmount,
  BigNumber.from(500)
);
export const CharitySeed = calculateFeeAmount(MintAmount, BigNumber.from(200));
export const ResourceLiquiditySeed = calculateFeeAmount(
  MintAmount,
  BigNumber.from(1500)
);
export const OwnerTokenBalance = MintAmount.sub(
  DevMarketSeed.add(CharitySeed).add(ResourceLiquiditySeed)
);

export const thirtyDays = 30 * 24 * 60 * 60;

export function calculateFeeAmount(
  amount: BigNumber,
  bp: BigNumberish
): BigNumber {
  let fee: BigNumber = amount.mul(bp).div(10000);
  return fee;
}

export function calculateBP(
  dividend: BigNumber,
  divisor: BigNumber
): BigNumber {
  let bp: BigNumber = dividend
    .mul(10 ** 9)
    .mul(10000)
    .div(divisor)
    .div(10 ** 9);
  return bp;
}

export async function timeWarp(seconds: number) {
  await ethers.provider.send("evm_increaseTime", [seconds]);
  await ethers.provider.send("evm_mine", []);
}

/**
 *
 * @returns {Promise<Signer[], Contract>}
 */
export async function deployContract(): Promise<[Signer[], Contract]> {
  const accounts = await ethers.getSigners();

  let dev_market_wallet = await accounts[1].getAddress();
  let charity_wallet = await accounts[2].getAddress();
  let resource_liquidity = await accounts[3].getAddress();

  let contract_factory = await ethers.getContractFactory("RetroVisionToken");
  let contract = await contract_factory.deploy(
    TokenName,
    TokenSymbol,
    TokenDecimals,
    TotalSupply,
    dev_market_wallet,
    charity_wallet,
    resource_liquidity
  );

  return [accounts, contract];
}
