# Retro Vision Gamiverse Token

The RETROV token has been created to support the Retro Vision Gamiverse platform as it's monetary standard. The token will serve as the basis for value exchange when consuming hosting resources, NFTs and more. It will also
be utilized as leverage toward the governance of the ecosystem once we establish the DAO at a future stage.

The token has been developed from scratch with the intent to ensure compliance with our expected functionality
as well as a means to ensure flexability to support specific use-cases around a fair launch and market manipulations.
This is not to say that the token is perfect in preventing all forms of attack on the market, but we have put in
effort to stabalize as many common vectors as possible, while ensuring a predictable ramp to a full open market system.

## Tokenomics

The RETROV token has been created with the following economic standards.

- Total supply of 10,000,000,000 is to be minted at mainnet release
- 70% of the supply will be locked in to liquidity for an initial 6 months
  - To be extended to a minimum of 5 years once the intial lock lapses
- 8% to team vesting wllet 3m cliff, 3 year full vesting
- All purchases are governed by a 90 second cooldown, during this period outbound transfers are restricted. Anti arbitrage.
- **NEW** wallets will be subject to a 24 hour initial holding period, during which outbound transfers will be restricted
- All transfers initiated by non-excluded (1) wallets will be charged a fee according to the following schedule at launch
  - 3% to be redistributed to current token holders
  - 2% to be pooled toward resource provider liquidity
  - 2% to marketing and development funds
  - 2% to charity
- The fee schedule has an upper bound of 20%, to be adapted as market metrics change
- To help smooth the fairlaunch and give everyone more opportunity to purchase tokens, the following account caps and TX limits have been hard coded into the contract. All cap is based on total token supply, TX limit is a percentage of the current cap. Timer begins when the contract is made live on the network via a owner call to goLive() function.
  - Launch to 30 days - 0.5% holding cap, 10% TX limit
  - 30 to 60 days - 1% holding cap, 25% TX limit
  - 60 to 90 days - 5% holding cap, 50% TX limit
  - 90 to 180 days - 10% holding cap, 100% TX limit
  - 180 days or more - all limitations removed

<p style="font-size:-2pt; font-style:italic">
1) Excluded wallets are defined as wallets that have been flagged as special use. This includes but is not limited to internal project addresses, exchange wallets and other specific cases.
</p>

## Launch Checklist

- [ ] Deploy contract to mainnet
- [ ] Create liquidity pool
- [ ] Allocate funds to liquidity pool
- [ ] Configure liquidity pool in contract
- [ ]
- [ ]
- [ ]
- [ ]
- [ ]
- [ ]
- [ ]

## Owner Functions

### goLive()

One way function that when called enables public token transfer functionality within the contract.

### setAccountExcluded(address, bool, string)

Set the excluded state of the provided address, requires a reason passed as a string.

### setAccountLocked(address, bool, string)

Set the locked state of the provided address, requires a reason passed as a string.


### setFee(uint8, uint16)

Set the fee as basis point value (uint16) for fee defined by (uint8) type

Fee Types:

0) Reflection
1) Dev Marketing
2) Charity
3) Resource provider liquidity
4) DEX liquidity

### setInternalWalletAddress(uint8, address)

Set the address for internal wallet defined by (uint8) type. If the address is a contract address, the change becomes immutable and can not be reverted or changed in the future. This is to allow the transition to multi-sig or other secured mechanisms in the future and prevent tampering once the change has been made.

Wallet Types:

0) Dev Marketing
1) Charity
2) Resource provider liquidity
3) DEX liquidity
