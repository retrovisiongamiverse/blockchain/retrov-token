// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

//import "hardhat/console.sol";

import "./libraries/Utility.sol";

import "@openzeppelin/contracts/access/Ownable.sol";

import "@openzeppelin/contracts/interfaces/IERC20.sol";

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";

// Fee Schedule in BP - Initial total fee 13% maximum fee total is 20%
uint16 constant FEE_MAX_TOTAL = 2000;

// Indexes of internal wallets
uint8 constant WALLET_DEV_MARKET = 0;
uint8 constant WALLET_CHARITY = 1;
uint8 constant WALLET_RESOURCE_LIQUIDITY = 2;

uint8 constant FEE_REFLECTION = 0;
uint8 constant FEE_DEV_MARKET = 1;
uint8 constant FEE_CHARITY = 2;
uint8 constant FEE_RESOURCE_LIQUIDITY = 3;

uint256 constant FIRST_TX_DELAY = 1 days;
uint256 constant TX_DELAY = 90;

struct TokenParams {
    string name;
    string symbol;
    uint8 decimals;
    uint256 totalSupply;
    uint256 goLiveDate;
}

struct Account {
    uint256 balance;
    bool excluded;
    bool locked;
    uint256 lastReflectedTick;
    uint256 lastTX;
    uint256 firstTX;
    mapping(address => uint256) allowances;
}

contract RetroVisionToken is IERC20, Context, ReentrancyGuard, Ownable {
    using Address for address;

    using SafeMath for uint256;
    using SafeMath for uint16;
    using SafeMath for uint8;

    using Counters for Counters.Counter;

    TokenParams private _tokenParams;

    // Wallets
    mapping(uint8 => address) private _internalWallets;
    mapping(uint8 => bool) private _immutableWallet;

    // Tracking of fee schedules
    mapping(uint8 => uint16) private _feeSchedule;

    // Reflections
    uint256 private _totalReflections;
    Counters.Counter private _totalReflectionTicks;

    mapping(uint8 => uint256) private _totalFeesCollected;

    // Accounting of account states
    mapping(address => Account) private _accounts;

    // Token limits
    uint256[] private _limitCapSchedule;
    uint256[] private _limitTxSchedule;

    /**
     * Constructor
     */
    constructor(
        string memory _name,
        string memory _symbol,
        uint8 _decimals,
        uint256 _supply,
        address _devMarketWallet,
        address _charityWallet,
        address _resourceLiquidityWallet
    ) {
        // Initialize token parameters
        _tokenParams.name = _name;
        _tokenParams.symbol = _symbol;
        _tokenParams.decimals = _decimals;
        _tokenParams.totalSupply = _supply.mul(10**uint256(_decimals)); // Convert to retrowei

        _totalReflections = 0;
        _totalReflectionTicks.increment();

        _totalFeesCollected[FEE_REFLECTION] = 0;
        _totalFeesCollected[FEE_DEV_MARKET] = 0;
        _totalFeesCollected[FEE_CHARITY] = 0;
        _totalFeesCollected[FEE_RESOURCE_LIQUIDITY] = 0;

        // Initial token limits
        _limitCapSchedule = [
            Finance.calculateFeeAmount(_tokenParams.totalSupply, 50), // 0.5%
            Finance.calculateFeeAmount(_tokenParams.totalSupply, 100), // 1.0%
            Finance.calculateFeeAmount(_tokenParams.totalSupply, 500), // 5.0%
            Finance.calculateFeeAmount(_tokenParams.totalSupply, 1000) // 10.0%
        ];

        // 10%, 25%, 50%, 100%
        _limitTxSchedule = [1000, 2500, 5000, 10000];

        // Initi wallet addresses
        _intitalizeAccount(_msgSender());
        _intitalizeAccount(_devMarketWallet);
        _intitalizeAccount(_charityWallet);
        _intitalizeAccount(_resourceLiquidityWallet);

        // Initialize internal wallets
        setInternalWalletAddress(WALLET_DEV_MARKET, _devMarketWallet);
        setInternalWalletAddress(WALLET_CHARITY, _charityWallet);
        setInternalWalletAddress(
            WALLET_RESOURCE_LIQUIDITY,
            _resourceLiquidityWallet
        );

        // Set initial fees
        setFee(FEE_REFLECTION, 300);
        setFee(FEE_DEV_MARKET, 200);
        setFee(FEE_CHARITY, 200);
        setFee(FEE_RESOURCE_LIQUIDITY, 200);

        // Calculate seed amounts 5% + 2% + 15% = 22%
        uint256 _devMarketSeed = Finance.calculateFeeAmount(
            _tokenParams.totalSupply,
            500
        ); // 5%
        uint256 _charitySeed = Finance.calculateFeeAmount(
            _tokenParams.totalSupply,
            200
        ); // 5%
        uint256 _resourceLiquiditySeed = Finance.calculateFeeAmount(
            _tokenParams.totalSupply,
            1500
        ); // 15%

        // Remaining 65% to Liquidity and 10% to Vesting Wallet
        uint256 _remainingSeed = _tokenParams.totalSupply.sub(
            _devMarketSeed.add(_charitySeed).add(_resourceLiquiditySeed)
        );

        // Seed distributions
        _accounts[_devMarketWallet].balance = _devMarketSeed;
        _accounts[_charityWallet].balance = _charitySeed;
        _accounts[_resourceLiquidityWallet].balance = _resourceLiquiditySeed;
        _accounts[_msgSender()].balance = _remainingSeed;

        // Exclude addresses from tax calculations
        setAccountExcluded(
            _msgSender(),
            true,
            "Constructor: Owner address exclusion"
        );
        setAccountExcluded(
            _devMarketWallet,
            true,
            "Constructor: Dev Marketing address exclusion"
        );
        setAccountExcluded(
            _charityWallet,
            true,
            "Constructor: Charity address exclusion"
        );
        setAccountExcluded(
            _resourceLiquidityWallet,
            true,
            "Constructor: Resource Liquidity address exclusion"
        );

        emit Transfer(address(0), _devMarketWallet, _devMarketSeed);
        emit Transfer(address(0), _charityWallet, _charitySeed);
        emit Transfer(
            address(0),
            _resourceLiquidityWallet,
            _resourceLiquiditySeed
        );
        emit Transfer(address(0), _msgSender(), _remainingSeed);
    }

    // TODO: What to do here, accept, return, or reject?
    receive() external payable {
        revert("Generic receive");
    }

    fallback() external payable {
        revert("Generic fallback");
    }

    function name() public view virtual returns (string memory) {
        return _tokenParams.name;
    }

    function symbol() public view virtual returns (string memory) {
        return _tokenParams.symbol;
    }

    function decimals() public view virtual returns (uint8) {
        return _tokenParams.decimals;
    }

    function totalSupply() public view override returns (uint256) {
        return _tokenParams.totalSupply;
    }

    function isLive() public view returns (bool) {
        return _tokenParams.goLiveDate != 0;
    }

    function goLive() public onlyOwner {
        require(!isLive(), "ERC20: Token already live");
        _tokenParams.goLiveDate = block.timestamp;

        emit GoLive();
    }

    function renounceOwnership() public virtual override onlyOwner {
        setAccountExcluded(
            owner(),
            false,
            "transferOwnership: Owner address exclusion removed"
        );

        _transferOwnership(address(0));
    }

    function transferOwnership(address newOwner)
        public
        virtual
        override
        onlyOwner
    {
        require(
            newOwner != address(0),
            "Ownable: new owner is the zero address"
        );

        setAccountExcluded(
            owner(),
            false,
            "transferOwnership: Owner address exclusion removed"
        );

        setAccountExcluded(
            newOwner,
            true,
            "transferOwnership: Owner address exclusion added"
        );

        _transferOwnership(newOwner);
    }

    function reflectionBalanceOf(address _account)
        public
        view
        returns (uint256)
    {
        uint256 _currentReflectionTick = _totalReflectionTicks.current();

        // Return 0 if account is excluded, there are no reflections
        if (
            isExcluded(_account) ||
            _totalReflections == 0 ||
            _currentReflectionTick == 0 ||
            _accounts[_account].lastReflectedTick >= _currentReflectionTick
        ) return 0;

        // Reflection balance or "owed" is equal to a percentage of the totalReflections
        // as the accounts ownership share of the total supply minus the total reflections paid
        // RB = (TR * (O / TS)) - RP

        uint256 _ownershipShareBP = Finance.calculateBP(
            _accounts[_account].balance,
            _tokenParams.totalSupply
        );

        uint256 _reflectionShareBP = Finance.calculateBP(
            _currentReflectionTick.sub(_accounts[_account].lastReflectedTick),
            _currentReflectionTick
        );

        uint256 _reflectionShare = Finance.calculateFeeAmount(
            _totalReflections,
            _reflectionShareBP
        );
        uint256 _reflectionOwed = Finance.calculateFeeAmount(
            _reflectionShare,
            _ownershipShareBP
        );

        return _reflectionOwed;
    }

    function balanceOf(address _account)
        public
        view
        override
        returns (uint256)
    {
        uint256 _tokensOwned = _accounts[_account].balance;
        if (!isExcluded(_account)) {
            _tokensOwned = _tokensOwned.add(reflectionBalanceOf(_account));
        }

        return _tokensOwned;
    }

    function setAccountExcluded(
        address _account,
        bool _excluded,
        string memory _reason
    ) public onlyOwner {
        require(isExcluded(_account) != _excluded, "ERC20: No change");

        _accounts[_account].excluded = _excluded;

        emit AccountExclusionStatusChanged(_account, _excluded, _reason);
    }

    function isExcluded(address _account) public view returns (bool) {
        return _accounts[_account].excluded;
    }

    function setAccountLocked(
        address _account,
        bool _locked,
        string memory _reason
    ) public onlyOwner {
        require(_accounts[_account].locked != _locked, "ERC20: No change");

        _accounts[_account].locked = _locked;

        emit AccountLockedStatusChanged(_account, _locked, _reason);
    }

    function isLocked(address _account) public view returns (bool) {
        return _accounts[_account].locked;
    }

    function totalReflections() public view returns (uint256) {
        return _totalReflections;
    }

    function allowance(address _owner, address _spender)
        public
        view
        override
        returns (uint256)
    {
        return _accounts[_owner].allowances[_spender];
    }

    function increaseAllowance(address _spender, uint256 _amount)
        public
        virtual
        returns (bool)
    {
        address _sender = _msgSender();
        _approve(
            _sender,
            _spender,
            _accounts[_sender].allowances[_spender].add(_amount)
        );
        return true;
    }

    function decreaseAllowance(address _spender, uint256 _amount)
        public
        virtual
        returns (bool)
    {
        address _sender = _msgSender();
        _approve(
            _sender,
            _spender,
            _accounts[_sender].allowances[_spender].sub(
                _amount,
                "ERC20: decreased allowance below zero"
            )
        );
        return true;
    }

    function approve(address _spender, uint256 _amount)
        public
        override
        nonReentrant
        returns (bool)
    {
        _approve(_msgSender(), _spender, _amount);
        return true;
    }

    function _approve(
        address _owner,
        address _spender,
        uint256 _amount
    ) private {
        require(_owner != address(0), "ERC20: approve from the zero address");
        require(_spender != address(0), "ERC20: approve to the zero address");

        _accounts[_owner].allowances[_spender] = _amount;
        emit Approval(_owner, _spender, _amount);
    }

    function transferFrom(
        address _spender,
        address _recipient,
        uint256 _amount
    ) public override returns (bool) {
        address _sender = _msgSender();
        _transfer(_spender, _recipient, _amount);
        _approve(
            _spender,
            _sender,
            _accounts[_spender].allowances[_sender].sub(
                _amount,
                "ERC20: transfer amount exceeds allowance"
            )
        );
        return true;
    }

    function transfer(address _recipient, uint256 _amount)
        public
        override
        returns (bool)
    {
        _transfer(_msgSender(), _recipient, _amount);
        return true;
    }

    function _intitalizeAccount(address _account) private {
        // Set initial tick on new holder account, to set their stake against the total supply
        if (_accounts[_account].firstTX == 0) {
            _accounts[_account].firstTX = block.timestamp;
            _accounts[_account].lastTX = block.timestamp;
            _accounts[_account].lastReflectedTick = _totalReflectionTicks
                .current()
                .add(1);

            emit AccountCreated(_account);
        }
    }

    function _applyReflectedFunds(address _account)
        internal
        nonReentrant
        returns (uint256)
    {
        if (isExcluded(_account)) return 0;

        uint256 _reflectionOwed = reflectionBalanceOf(_account);

        _accounts[_account].balance = _accounts[_account].balance.add(
            _reflectionOwed
        );

        _totalReflections = _totalReflections.sub(_reflectionOwed);

        _accounts[_account].lastReflectedTick = _totalReflectionTicks
            .current()
            .add(1);

        emit ReflectionApplied(_account, _reflectionOwed);

        return _reflectionOwed;
    }

    function _transfer(
        address _sender,
        address _recipient,
        uint256 _amount
    ) private {
        if (_sender != owner()) {
            // If pre-golive we emit event and revert TX, only owner can tranfer tokens to other accounts
            // such as a Liquidity pool, vesting contracts or other startup accounts
            if (!isLive()) {
                emit EarlyTransferFailed(_sender, _recipient, _amount);
                revert("ERC20: token not yet live on-chain");
            }

            if (!isExcluded(_sender)) {
                // TX delay enforced on non-excluded accounts for FIRST_TX_DELAY after account creation
                require(
                    block.timestamp.sub(_accounts[_sender].firstTX) >
                        FIRST_TX_DELAY,
                    "ERC20: transfer failed, intial wait period not met"
                );

                // TX delay enforced on non-excluded accounts
                require(
                    block.timestamp.sub(_accounts[_sender].lastTX) > TX_DELAY,
                    "ERC20: transfer failed, arbitrage delay not met"
                );
            }
        }

        // Reject TX for locked accounts
        require(
            !_accounts[_sender].locked && !_accounts[_recipient].locked,
            "ERC20: transfer failed, account is locked"
        );

        // After fees taken and reflection ticks have incremented we apply reflection balance to sender
        _applyReflectedFunds(_sender);

        // Initialize accounts if not yet initialized
        _intitalizeAccount(_sender);
        _intitalizeAccount(_recipient);

        uint256 _senderBalance = balanceOf(_sender);
        require(
            _amount <= _senderBalance,
            "ERC20: transfer amount exceeds senders balance"
        );
        require(
            _amount > 0,
            "ERC20: transfer amount must be greater than zero"
        );
        require(_sender != _recipient, "ERC20: transfer to self");

        // TODO: Transfer to Zero would equate to a burn, how should this be handled?
        require(_sender != address(0), "ERC20: transfer from the zero address");

        uint256 _finalAmount = _amount;
        if (!isExcluded(_sender)) {
            if (isLive() && _sender != owner()) {
                require(
                    _accounts[_recipient].balance.add(_amount) <=
                        getHoldingCap(_recipient),
                    "ERC20: transfer will exceed recipient holding cap"
                );
                require(
                    _amount <= getTXCap(_sender),
                    "ERC20: transfer amount exceeds TX cap"
                );
            }

            // Apply TX fees to TX when sender or recipient is a contract
            // Covers routers and other defi contracts
            // Liquidity address will be gated by exclusion list once contract is set as internal
            if (_sender.isContract() || _recipient.isContract()) {
                _finalAmount = _amount.sub(_applyFees(_amount));
            }
        }

        _accounts[_sender].balance = _accounts[_sender].balance.sub(
            _amount,
            "ERC20: transfer amount exceeds balance"
        );

        uint256 _recipientBalance = _accounts[_recipient].balance;
        _accounts[_recipient].balance = _recipientBalance.add(_finalAmount);

        _accounts[_sender].lastTX = block.timestamp;
        _accounts[_recipient].lastTX = block.timestamp;

        emit Transfer(_sender, _recipient, _finalAmount);
    }

    /**
     * @notice Applies fees for a given amount of tokens to internally manage wallets
     * @param _amount The amount of tokens to collect fees for.
     * @return The total fees collected.
     */
    function _applyFees(uint256 _amount) private returns (uint256) {
        // Calculate fees based on sender address
        (
            uint256 _reflectionFee,
            uint256 _devMarketFee,
            uint256 _charityFee,
            uint256 _resourceLiquidityFee,
            uint256 _totalFee
        ) = calculateFeeAmounts(_amount);

        // Get current balances
        uint256 _devMarketBalance = _accounts[
            _internalWallets[WALLET_DEV_MARKET]
        ].balance;
        uint256 _charityBalance = _accounts[_internalWallets[WALLET_CHARITY]]
            .balance;
        uint256 _resourceLiquidityBalance = _accounts[
            _internalWallets[WALLET_RESOURCE_LIQUIDITY]
        ].balance;

        // Reflection
        _totalReflections = _totalReflections.add(_reflectionFee);
        _totalReflectionTicks.increment();
        _totalFeesCollected[FEE_REFLECTION] = _totalFeesCollected[
            FEE_REFLECTION
        ].add(_reflectionFee);

        emit ReflectedAmount(_reflectionFee, _totalReflectionTicks.current());

        // Dev Market
        _accounts[_internalWallets[WALLET_DEV_MARKET]]
            .balance = _devMarketBalance.add(_devMarketFee);

        _totalFeesCollected[FEE_DEV_MARKET] = _totalFeesCollected[
            FEE_DEV_MARKET
        ].add(_devMarketFee);

        emit DevMarketingFeeApplied(_devMarketFee);

        // Charity
        _accounts[_internalWallets[WALLET_CHARITY]].balance = _charityBalance
            .add(_charityFee);

        _totalFeesCollected[FEE_CHARITY] = _totalFeesCollected[FEE_CHARITY].add(
            _charityFee
        );

        emit CharityFeeApplied(_charityFee);

        // Resource Liquidity
        _accounts[_internalWallets[WALLET_RESOURCE_LIQUIDITY]]
            .balance = _resourceLiquidityBalance.add(_resourceLiquidityFee);

        _totalFeesCollected[FEE_RESOURCE_LIQUIDITY] = _totalFeesCollected[
            FEE_RESOURCE_LIQUIDITY
        ].add(_resourceLiquidityFee);

        emit ResourceLiquidityFeeApplied(_resourceLiquidityFee);

        return _totalFee;
    }

    /**
     * Fee management
     */

    /**
     * @notice Calculate and return values for current fee schedule based on amount and address exclusion
     * @param _amount Amount of tokens to calculate fees for
     * @return reflection_fee, dev_market_fee, charity_fee, resource_liquidity_fee, liquidity_fee, total_fee
     */
    function calculateFeeAmounts(uint256 _amount)
        internal
        view
        returns (
            uint256,
            uint256,
            uint256,
            uint256,
            uint256
        )
    {
        uint256 _reflectionFee = Finance.calculateFeeAmount(
            _amount,
            _feeSchedule[FEE_REFLECTION]
        );
        uint256 _devMarketFee;
        uint256 _charityFee;
        uint256 _resourceLiquidityFee;

        if (_internalWallets[WALLET_DEV_MARKET] != address(0)) {
            _devMarketFee = Finance.calculateFeeAmount(
                _amount,
                _feeSchedule[FEE_DEV_MARKET]
            );
        }

        if (_internalWallets[WALLET_CHARITY] != address(0)) {
            _charityFee = Finance.calculateFeeAmount(
                _amount,
                _feeSchedule[FEE_CHARITY]
            );
        }

        if (_internalWallets[WALLET_RESOURCE_LIQUIDITY] != address(0)) {
            _resourceLiquidityFee = Finance.calculateFeeAmount(
                _amount,
                _feeSchedule[FEE_RESOURCE_LIQUIDITY]
            );
        }

        uint256 _totalFee = _reflectionFee
            .add(_devMarketFee)
            .add(_charityFee)
            .add(_resourceLiquidityFee);

        return (
            _reflectionFee,
            _devMarketFee,
            _charityFee,
            _resourceLiquidityFee,
            _totalFee
        );
    }

    /**
     * @notice Return the current fee schedule
     * @return The current fee schedule (reflection, dev market, charity, resource liquidity)
     */
    function getFees()
        public
        view
        returns (
            uint256,
            uint256,
            uint256,
            uint256
        )
    {
        return (
            _feeSchedule[FEE_REFLECTION],
            _feeSchedule[FEE_DEV_MARKET],
            _feeSchedule[FEE_CHARITY],
            _feeSchedule[FEE_RESOURCE_LIQUIDITY]
        );
    }

    /**
     * @notice Returns the previous and current fee from the schedule for the requested fee type
     * @param _feeType UINT of the FeeTypes ENUM you are requesting
     * @return current fee amount in basis points % = value / 1000
     */
    function getFee(uint8 _feeType) public view returns (uint256) {
        return (_feeSchedule[_feeType]);
    }

    /**
     * @notice Update fee schedule, setting _feeType to new value
     * @param _feeType UINT of the FeeTypes ENUM you are updating
     * @param _newFee UINT of the new fee as a value in basis points
     */
    function setFee(uint8 _feeType, uint16 _newFee) public onlyOwner {
        require(
            (
                _feeSchedule[FEE_REFLECTION]
                    .add(_feeSchedule[FEE_DEV_MARKET])
                    .add(_feeSchedule[FEE_CHARITY])
                    .add(_feeSchedule[FEE_RESOURCE_LIQUIDITY])
                    .add(_newFee)
            ).sub(_feeSchedule[_feeType]) <= FEE_MAX_TOTAL,
            "ERC20: proposed fee schedule exceeds limit of 20%"
        );

        uint16 _oldFee = _feeSchedule[_feeType];
        require(_newFee != _oldFee, "ERC20: fee already set");

        // Update current schedule
        _feeSchedule[_feeType] = _newFee;

        emit FeeUpdated(_feeType, _oldFee, _newFee);
    }

    /**
     * @notice Returns the lifetime totals for each fee type
     * @return total_collected_reflection_fees, total_collected_dev_market_fees,
     *         total_collected_charity_fees, total_collected_resource_liquidity_fees
     */
    function getTotalFeesCollected()
        public
        view
        returns (
            uint256,
            uint256,
            uint256,
            uint256
        )
    {
        return (
            _totalFeesCollected[FEE_REFLECTION],
            _totalFeesCollected[FEE_DEV_MARKET],
            _totalFeesCollected[FEE_CHARITY],
            _totalFeesCollected[FEE_RESOURCE_LIQUIDITY]
        );
    }

    /**
     * @notice Fetch the current balance for the requested internal wallet
     * @return balance in Token Units
     */
    function getInternalBalance(uint8 _walletType)
        public
        view
        returns (uint256)
    {
        return _accounts[_internalWallets[_walletType]].balance;
    }

    /**
     * @notice Fetch the current address for the requested internal wallet
     * @return address and mutability of the internal wallet
     */
    function getInternalWalletAddress(uint8 _walletType)
        public
        view
        returns (address, bool)
    {
        return (_internalWallets[_walletType], _immutableWallet[_walletType]);
    }

    /**
     * @notice Allow contract owner to update internal addresses in the event of breach or other event.
     * Once project matures this is to be set to a form of contract based wallet at which point that wallet
     * type becomes immutable.
     * @param _walletType the type of internal wallet we are updating the address for
     * @param _newAddress the new address to set the internal wallet too. Set to contract address to freeze type
     */
    function setInternalWalletAddress(uint8 _walletType, address _newAddress)
        public
        onlyOwner
    {
        require(
            _newAddress != address(0),
            "ERC20: internal wallet must not be 0"
        );
        require(
            _newAddress != address(this),
            "ERC20: internal wallet must not be this contract"
        );
        require(
            _newAddress != _internalWallets[_walletType],
            "ERC20: new address must not be same"
        );
        require(
            !_immutableWallet[_walletType],
            "ERC20: internal address is immutable"
        );

        address _oldAddress = _internalWallets[_walletType];
        _internalWallets[_walletType] = _newAddress;

        emit InternalWalletAddressChanged(
            _walletType,
            _oldAddress,
            _newAddress
        );

        // Automated transfer of all tokens to new address
        _accounts[_newAddress].balance = _accounts[_oldAddress].balance;
        _accounts[_oldAddress].balance = 0;

        emit Transfer(_oldAddress, _newAddress, _accounts[_newAddress].balance);

        // Freeze this internal wallet type from future change, hope you meant to do this!
        if (_newAddress.isContract()) {
            _immutableWallet[_walletType] = true;
            emit InternalWalletAddressMadeImmutable(
                _walletType,
                _oldAddress,
                _newAddress
            );
        }
    }

    /**
     * @notice Fetch the current maximum holding cap based on the age of the contract
     * @return number of tokens account may hold as WEI
     */
    function getHoldingCap(address _account) public view returns (uint256) {
        if (isExcluded(_account)) {
            return _tokenParams.totalSupply;
        }

        if (!isLive()) return 0;

        uint256 _seconds = block.timestamp.sub(_tokenParams.goLiveDate);

        // After 6 months remove all holding caps
        if (_seconds > 180 days) return _tokenParams.totalSupply;

        return _limitCapSchedule[Math.min((_seconds.div(30 days)), 3)];
    }

    /**
     * @notice Fetch the current maximum TX cap based on the age of the contract
     * @return number of tokens account may hold as WEI
     */
    function getTXCap(address _account) public view returns (uint256) {
        if (isExcluded(_account)) {
            return _tokenParams.totalSupply;
        }

        if (!isLive()) return 0;

        uint256 _seconds = block.timestamp.sub(_tokenParams.goLiveDate);
        uint256 _limit = _limitTxSchedule[Math.min((_seconds.div(30 days)), 3)];
        uint256 _holdingCap = getHoldingCap(_account);

        return Finance.calculateFeeAmount(_holdingCap, _limit);
    }

    /**
     * @notice Fetch the number of seconds remaining until address is eligible to make a transaction
     * @return Seconds remaining until address is eligible to make a transaction
     */
    function getRemainingTXWait(address _account)
        public
        view
        returns (uint256)
    {
        if (isExcluded(_account)) {
            return 0;
        }

        // Initial delay
        if (block.timestamp.sub(_accounts[_account].firstTX) < FIRST_TX_DELAY) {
            uint256 _initialWait = block.timestamp.sub(
                _accounts[_account].firstTX
            );
            return
                _initialWait > FIRST_TX_DELAY
                    ? 0
                    : FIRST_TX_DELAY.sub(_initialWait);
        }
        // Arbitrage delay, we are already past initial delay
        else if (block.timestamp.sub(_accounts[_account].lastTX) < TX_DELAY) {
            uint256 _arbitrageWait = block.timestamp.sub(
                _accounts[_account].lastTX
            );
            return _arbitrageWait > TX_DELAY ? 0 : TX_DELAY.sub(_arbitrageWait);
        }

        return 0;
    }

    /*************************************************************************
     * Events
     *************************************************************************/

    // Emmitted when a fee is updated
    event FeeUpdated(uint8 _feeType, uint16 _oldFee, uint16 _newFee);

    // Emmitted when fee has been applied ie: transferred to wallet
    event DevMarketingFeeApplied(uint256 _amount);
    event CharityFeeApplied(uint256 _amount);
    event ResourceLiquidityFeeApplied(uint256 _amount);

    // Emitted when reflection has been collected to reflection pool
    event ReflectedAmount(uint256 _amount, uint256 _ticks);

    // Emitted when an account has performed an action that results in a reflection
    // liquidity event committing their reflection share to their wallet
    event ReflectionApplied(address indexed _account, uint256 _reflectionOwed);

    // Emmitted when an internal wallet has been updated
    event InternalWalletAddressChanged(
        uint8 _walletType,
        address indexed _oldAddress,
        address indexed _newAddress
    );

    // Emmited when an internal wallet converts to a contract wallet and becomes immutable
    event InternalWalletAddressMadeImmutable(
        uint8 _walletType,
        address indexed _oldAddress,
        address indexed _newAddress
    );

    // Emmitted when a new account has been added to the contract
    event AccountCreated(address indexed _account);

    // Emmitted when an account has been excluded from fees
    event AccountExclusionStatusChanged(
        address indexed _account,
        bool _excluded,
        string _reason
    );

    // Emmitted when an account's locked status has been changed
    event AccountLockedStatusChanged(
        address indexed _account,
        bool _locked,
        string _reason
    );

    // Emmitted when the token goLive has been called and the token TX mechanism has been made globally active
    event GoLive();

    // Emmitted when the transfer function called prior to goLive
    event EarlyTransferFailed(
        address indexed _from,
        address indexed _to,
        uint256 _value
    );
}
