// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Test is ERC20 {
    uint8 private _decimals;

    constructor() ERC20("Test", "TST") {
        _decimals = 18;
    }
}
