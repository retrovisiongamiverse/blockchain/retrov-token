// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
pragma abicoder v2;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";

library Finance {
    using SafeMath for uint256;

    /**
     * @notice Calculates the amount of a fee given as basis points
     * @param _amount The amount to calculate the fee for
     * @param _bp The fee in basis points to calculate
     */
    function calculateFeeAmount(uint256 _amount, uint256 _bp)
        internal
        pure
        returns (uint256)
    {
        uint256 _fee = _amount.mul(_bp).div(10000);

        return _fee;
    }

    /**
     * @notice Calculates basis points
     * @param _dividend The value to calculate the basis points for
     * @param _divisor The value to divide the dividend by, generally the whole amount
     */
    function calculateBP(uint256 _dividend, uint256 _divisor)
        internal
        pure
        returns (uint256)
    {
        uint256 _bp = _dividend.mul(10**9).mul(10000).div(_divisor).div(10**9);
        return _bp;
    }
}
