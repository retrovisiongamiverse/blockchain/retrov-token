// deploy/00_deploy_retrov_contract.js

import {HardhatRuntimeEnvironment} from 'hardhat/types';
import {DeployFunction} from 'hardhat-deploy/types';
import { BigNumber } from "ethers";

const TokenName = "RetroVision Gamiverse";
const TokenSymbol = "RETROV";
const TokenDecimals = 9;
const TotalSupply = BigNumber.from("1000000000");

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const {deployments, getNamedAccounts} = hre;
  const {deploy} = deployments;
  const {deployer, dev_market, charity, resource_liquidity} = await getNamedAccounts();

  await deploy('RetroVisionToken', {
    from: deployer,
    args: [TokenName, TokenSymbol, TokenDecimals, TotalSupply, dev_market, charity, resource_liquidity],
    log: true,
  });

  console.log(`Constructor ARGS: \"${TokenName}\" \"${TokenSymbol}\" ${TokenDecimals} ${TotalSupply} \"${dev_market}\" \"${charity}\" \"${resource_liquidity}\"`);
};

export default func;

module.exports.tags = ['RetroVisionToken'];
